package com.nopmind.incepth;

import org.apache.commons.io.FileUtils;
import spoon.Launcher;
import spoon.compiler.SpoonCompiler;
import spoon.reflect.declaration.CtClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Interface for Spoon framework.
 */
public class SpoonInterface {

    /**
     * Patches a class using the Spoon framework.
     *
     * @param decompiledClass
     *      decompiled class (source code)
     * @param patcher
     *      anonymous function which modifies the class
     * @param jarClass
     *      JarClass
     * @return
     *      modified compiled class
     * @throws IOException
     */
    public static byte[] patch(byte[] decompiledClass, Consumer<CtClass> patcher, JarClass jarClass) throws IOException {
        // make a new file and save the decompiled class
        Path tempDir = Files.createTempDirectory(null);

        File decompiledClassFile = new File(tempDir.resolve(jarClass.getClassName() + ".java").toString());
        FileUtils.writeByteArrayToFile(decompiledClassFile, decompiledClass);

        Launcher spoon = new Launcher();

        List<File> resources = jarClass.getRelatedJar().getDependencies();
        resources.add(jarClass.getRelatedJar().getFile());

        List<String> sourceClassPath = new ArrayList<>();
        for(File res : resources)
            sourceClassPath.add(res.getAbsolutePath());

        spoon.getEnvironment().setSourceClasspath(sourceClassPath.toArray(new String[sourceClassPath.size()]));

        spoon.addInputResource(decompiledClassFile.getAbsolutePath());
        spoon.buildModel();


        CtClass actualClass = spoon.getFactory().Class().get(jarClass.getClassPath());

        patcher.accept(actualClass);

        SpoonCompiler compiler = spoon.createCompiler();
        compiler.setBinaryOutputDirectory(new File(tempDir.toString()));
        compiler.setSourceOutputDirectory(new File(tempDir.toString()));

        compiler.compile();

        File patchedCompiledFile = new File(tempDir.resolve(jarClass.getEntryPath()).toString());
        byte[] patchedCompiledClass = Files.readAllBytes(patchedCompiledFile.toPath());

        FileUtils.deleteDirectory(tempDir.toFile());

        return patchedCompiledClass;
    }
}
