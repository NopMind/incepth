package com.nopmind.incepth;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

/**
 * Extends the JarFile with helper methods.
 */
public class JarFile extends java.util.jar.JarFile {
    /// the actual file
    private File file;
    /// dependencies - used whilest de/compiling
    private List<File> dependencies;

    /**
     * Constructor.
     *
     * @param filePath
     *      path to jarfile
     * @throws IOException
     *      file not valid
     */
    public JarFile(String filePath) throws IOException {
        super(filePath);
        this.file = new File(filePath);
        this.dependencies = new ArrayList<>();
    }

    /**
     * Constructor.
     *
     * @param file
     *      jarfile
     * @throws IOException
     *      jarfile not valid
     */
    public JarFile(File file) throws IOException {
        super(file);
        this.file = file;
        this.dependencies = new ArrayList<>();
    }

    public File getFile() {
        return this.file;
    }

    /**
     * Get a JarClass from classpath.
     *
     * @param classPath
     *      classpath to class
     * @return
     *      JarClass || null if class not found
     * @throws IOException
     */
    public JarClass getJarClass(String classPath) throws IOException {
        String entryPath = JarClass.classToEntryPath(classPath);
        JarEntry jarEntry = getEntryByName(entryPath);

        if(jarEntry == null)
            return null;

        return new JarClass(this, jarEntry);
    }

    /**
     * Get multiple JarClass by classpaths.
     *
     * @param classPaths
     *      list of classpaths
     * @return
     *      list of JarClass
     * @throws IOException
     */
    public JarClass[] getJarClasses(List<String> classPaths) throws IOException {
        JarEntry[] jarEntries = filterEntries((JarEntry entry) -> {
            if(classPaths.contains(JarClass.entryToClassPath(entry.getName())))
                return true;
            return false;
        });

        List<JarClass> jarClasses = new ArrayList<>();
        for(JarEntry entry : jarEntries)
            jarClasses.add(new JarClass(this, entry));

        return (JarClass[])jarClasses.toArray();
    }

    /**
     * Adds a dependency file used for compiling this jar.
     *
     * @param dependency
     *      dependency file
     * @throws IOException
     *      dependency not valid
     */
    public void addDependency(File dependency) throws IOException {
        if(dependency.exists() && dependency.isFile())
            this.dependencies.add(dependency);
        else
            throw new IOException(dependency.getAbsolutePath() + " is not a valid dependency.");
    }

    /**
     * Adds multiple dependency files.
     *
     * @param dependencies
     *      dependency array
     * @throws IOException
     *      dependency not valid
     */
    public void addDependencies(File[] dependencies) throws IOException {
        for(File dependency : dependencies)
            addDependency(dependency);
    }

    /**
     * Adds multiple dependency files.
     *
     * @param dependencies
     *      dependency list
     * @throws IOException
     *      dependency not valid
     */
    public void addDependencies(List<File> dependencies) throws IOException {
        addDependencies(dependencies.toArray(new File[dependencies.size()]));
    }

    /**
     * Get a list of all dependencies.
     *
     * @return
     *      dependency list
     */
    public List<File>  getDependencies() {
        return this.dependencies;
    }

    /**
     * Crawls through the entries of this jar.
     *
     * @param crawler
     *      anonymous function called for every JarEntry
     *
     * @throws IOException
     *
     */
    public void crawl(Consumer<JarEntry> crawler) throws IOException {
        for(ZipEntry entry : Collections.list(this.entries()))
            crawler.accept(new JarEntry(entry, this.getInputStream(entry)));
    }

    /**
     * Get a filtered list of entries.
     *
     * @param filter
     *      anonymous function for filtering entries
     * @return
     *      filtered entry array
     * @throws IOException
     */
    public JarEntry[] filterEntries(Function<JarEntry, Boolean> filter) throws IOException {
        List<JarEntry> entries = new ArrayList<>();

        for(ZipEntry entry : Collections.list(this.entries())) {
            JarEntry jarEntry = new JarEntry(entry, this.getInputStream(entry));
            if(filter.apply(jarEntry))
                entries.add(jarEntry);
        }

        return (JarEntry[])entries.toArray();
    }

    /**
     * Get a single JarEntry from file.
     *
     * @param filter
     *      anonymous function for filter entry
     * @return
     *      entry || null when not found
     * @throws IOException
     */
    public JarEntry getEntry(Function<JarEntry, Boolean> filter) throws IOException {
        for(ZipEntry entry : Collections.list(this.entries())) {
            JarEntry jarEntry = new JarEntry(entry, this.getInputStream(entry));
            if(filter.apply(jarEntry))
                return jarEntry;
        }

        return null;
    }

    /**
     * Get a single JarEntry by name.
     *
     * @param entryName
     *      name of entry
     * @return
     *      JarEntry || null when not found
     * @throws IOException
     */
    public JarEntry getEntryByName(String entryName) throws IOException {
        return getEntry((JarEntry entry) -> {
            if(entry.getName().equals(entryName))
                return true;
            return false;
        });
    }

    /**
     * Replaces entries of this jar.
     *
     * @param output
     *      output file
     * @param replacer
     *      anonymous function called for every JarEntry and returns JarEntry
     * @throws IOException
     */
    public void replace(File output, Function<JarEntry, JarEntry> replacer) throws IOException {
        if(!output.exists())
            output.createNewFile();

        JarOutputStream outputStream = new JarOutputStream(new FileOutputStream(output));

        outputStream.setLevel(9);
        outputStream.setMethod(8);

        for(ZipEntry entry : Collections.list(this.entries())) {
            JarEntry patched = replacer.apply(new JarEntry(entry, this.getInputStream(entry)));

            outputStream.putNextEntry(patched);
            writeStream(patched.getContentStream(), outputStream);
            outputStream.closeEntry();
        }

        outputStream.flush();
        outputStream.close();
    }

    /**
     * Replace entries and save output as new file.
     *
     * @param output
     *      output file
     * @param replacements
     *      map of replacements where
     *          key = entry name
     *          value = new entry
     * @throws IOException
     */
    public void replaceEntries(File output, Map<String, JarEntry> replacements) throws IOException {
        replace(output, (JarEntry entry) -> {
            if(replacements.containsKey(entry.getName()))
                return replacements.get(entry.getName());
            return entry;
        });
    }

    /**
     * Replace a single entry and save output.
     *
     * @param output
     *      output file
     * @param entryPath
     *      entry name
     * @param newEntry
     *      new entry
     * @throws IOException
     */
    public void replaceEntry(File output, String entryPath, JarEntry newEntry) throws IOException {
        Map<String, JarEntry> replacements = new HashMap<>();
        replacements.put(entryPath, newEntry);
        replaceEntries(output, replacements);
    }

    /**
     * Replace multiple JarClass and save output.
     *
     * @param output
     *      output file
     * @param jarClasses
     *      list of JarClass
     * @throws IOException
     */
    public void replaceClasses(File output, List<JarClass> jarClasses) throws IOException {
        Map<String, JarEntry> replacements = new HashMap<>();

        for(JarClass jarClass : jarClasses)
            replacements.put(jarClass.getEntryPath(), jarClass.getClassEntry());

        replaceEntries(output, replacements);
    }

    /**
     * Replace a single JarClass and save output.
     *
     * @param output
     *      output file
     * @param jarClass
     *      replacement
     * @throws IOException
     */
    public void replaceClass(File output, JarClass jarClass) throws IOException {
        List<JarClass> jarClasses = new ArrayList<>();
        jarClasses.add(jarClass);

        replaceClasses(output, jarClasses);
    }

    /**
     * Writes input stream to output stream.
     *
     * @param is
     *      input stream
     * @param os
     *      output stream
     */
    private static void writeStream(InputStream is, OutputStream os) {
        final Integer bufferSize = 4096;

        try {
            byte buffer[] = new byte[bufferSize];
            int length;
            while ((length = is.read(buffer)) > 0)
                os.write(buffer, 0, length);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
