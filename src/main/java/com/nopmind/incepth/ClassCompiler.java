package com.nopmind.incepth;

import com.strobel.assembler.metadata.JarTypeLoader;
import com.strobel.decompiler.Decompiler;
import com.strobel.decompiler.DecompilerSettings;
import com.strobel.decompiler.PlainTextOutput;
import java.io.StringWriter;

/**
 *  Java class compiler interface.
 */
public class ClassCompiler {
    /**
     * Decompiles a .class file from Jar.
     *
     * @param classpath
     *      classpath of class
     * @param jarFile
     *      JarFile
     * @return
     *      decompiled class
     */
    public static byte[] decompile(String classpath, JarFile jarFile) {
        DecompilerSettings settings = DecompilerSettings.javaDefaults();
        settings.setTypeLoader(
                new JarTypeLoader(jarFile)
        );
        settings.setForceExplicitImports(true);

        StringWriter decompiledClass = new StringWriter();
        Decompiler.decompile(classpath.replace(".", "/"), new PlainTextOutput(decompiledClass), settings);

        return decompiledClass.toString().getBytes();
    }
}
