package com.nopmind.incepth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;

/**
 * Represents an entry in a Jar file.
 */
public class JarEntry extends ZipEntry {
    // the content (actual file) of entry
    private byte[] content;

    /**
     * Constructor.
     *
     * @param entry
     *      ZipEntry
     * @param fileStream
     *      InputStream of entry content
     * @throws IOException
     */
    public JarEntry(ZipEntry entry, InputStream fileStream) throws IOException {
        super(entry);
        // set compressed size to unknown
        this.setCompressedSize(-1);
        //this.content = IOUtils.toByteArray(fileStream);
        this.content = new byte[]{};
    }

    /**
     * Get the content of entry.
     *
     * @return
     *      content of entry
     */
    public byte[] getContent() {
        return this.content;
    }

    /**
     * Overwrite content of entry.
     *
     * @param content
     *      new content
     */
    public void setContent(byte[] content) {
        this.content = content;
        this.setSize(content.length);
    }

    /**
     * Get content input stream.
     *
     * @return
     *      InputStream of content
     */
    public InputStream getContentStream() {
        return new ByteArrayInputStream(this.content);
    }


}
