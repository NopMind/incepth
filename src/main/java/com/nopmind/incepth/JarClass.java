package com.nopmind.incepth;

import spoon.reflect.declaration.CtClass;
import java.io.IOException;
import java.util.function.Consumer;

/**
 *  Represents a class file packed in a jar.
 */
public class JarClass {
    // the JarEntry of class
    private JarEntry classEntry;
    // the Jar containing this class
    private JarFile relatedJar;

    /**
     * Convert an JarEntry path to a classpath.
     *
     * @param entryPath
     *      JarEntry path
     * @return
     *      classpath
     */
    public static String entryToClassPath(String entryPath) {
        return entryPath.replace("/", ".").replace(".class", "");
    }

    /**
     * Convert an classpath to a JarEntry path.
     *
     * @param classPath
     *      classpath
     * @return
     *      JarEntry path
     */
    public static String classToEntryPath(String classPath) {
        return classPath.replace(".", "/") + ".class";
    }

    /**
     * Constructor.
     *
     * @param relatedJar
     *      the JarFile containing this entry
     * @param classEntry
     *      the JarEntry containing this class
     */
    public JarClass(JarFile relatedJar, JarEntry classEntry) {
        this.classEntry = classEntry;
        this.relatedJar = relatedJar;
    }

    /**
     * Get the JarEntry of this class.
     *
     * @return
     *      JarEntry of this class
     */
    public JarEntry getClassEntry() {
        return this.classEntry;
    }

    /**
     * Get the JarFile containing this entry.
     *
     * @return
     *      JarFile containing this class
     */
    public JarFile getRelatedJar() {
        return this.relatedJar;
    }

    /**
     * Get the classpath of this class.
     *
     * @return
     *      classpath
     */
    public String getClassPath() {
        return entryToClassPath(classEntry.getName());
    }

    /**
     * Get the entry path of this class.
     *
     * @return
     *      entry path
     */
    public String getEntryPath() {
        return this.classEntry.getName();
    }

    /**
     * Get the classname of this class.
     *
     * @return
     *      classname
     */
    public String getClassName() {
        return getClassPath().substring(getClassPath().lastIndexOf(".")+1);
    }

    /**
     * Patch this class (aka modify and overwrite this class).
     *
     * @param patcher
     *      anonymous function receiving a CtClass which modifies this class
     * @return
     *      patched JarClass
     * @throws IOException
     */
    public JarClass patch(Consumer<CtClass> patcher) throws IOException {
        byte[] decompiledClass = ClassCompiler.decompile(this.getClassPath(), this.relatedJar);
        byte[] patchedClass = SpoonInterface.patch(decompiledClass, patcher, this);

        JarEntry patchedEntry = this.classEntry;
        patchedEntry.setContent(patchedClass);

        return new JarClass(this.relatedJar, patchedEntry);
    }
}
